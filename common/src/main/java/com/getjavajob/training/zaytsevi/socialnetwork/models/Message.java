package com.getjavajob.training.zaytsevi.socialnetwork.models;

import javax.persistence.*;
import java.sql.Timestamp;

@MappedSuperclass
public class Message {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected long id;
    @Column(name = "date", nullable = false, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    protected Timestamp date;
    protected String text;
    @Column(name = "image_url")
    protected String imageUrl;
    @ManyToOne
    @JoinColumn(name = "from_smn")
    protected Account from;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Timestamp getDate() {
        return date;
    }

    public void setDate(Timestamp date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Account getFrom() {
        return from;
    }

    public void setFrom(Account from) {
        this.from = from;
    }

    public String getDateString() {
        String preResult = date.toString();
        StringBuilder result = new StringBuilder();
        result.append(preResult.substring(8, 10)).append('.').append(preResult.substring(5, 7)).append('.')
                .append(preResult.substring(0, 4)).append(' ').append(preResult.substring(11, 13)).append(':')
                .append(preResult.substring(14, 16));
        return result.toString();
    }
}

package com.getjavajob.training.zaytsevi.socialnetwork.models;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "profile_messages")
public class ProfileMessage extends Message {

    @ManyToOne
    @JoinColumn(name = "to_smn")
    protected Account to;

    public ProfileMessage() {
        //Default constructor for JPA
    }

    public ProfileMessage(Account from, Account to, String text, String imageUrl) {
        this.from = from;
        this.to = to;
        this.text = text;
        this.imageUrl = imageUrl;
    }

    public ProfileMessage(String from, String to, String text, String imageUrl) {
        this.from = new Account(from);
        this.to = new Account(to);
        this.text = text;
        this.imageUrl = imageUrl;
    }

    public Account getTo() {
        return to;
    }

    public void setTo(Account to) {
        this.to = to;
    }
}

package com.getjavajob.training.zaytsevi.socialnetwork.models;

import javax.persistence.*;
import java.sql.Date;
import java.util.Base64;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "groups")
public class Group {
    @Id
    @Column(name = "group_id")
    private String id;
    @Column(name = "group_name")
    private String name;
    @Column(name = "description")
    private String desc;
    @ManyToOne
    @JoinColumn(name = "owner")
    private Account owner;
    @Column(name = "image")
    private byte[] image;
    @ManyToMany(mappedBy = "groups")
    private List<Account> members;
    @Column(name = "creation_date", nullable = false, updatable = false, insertable = false, columnDefinition = "TIMESTAMP DEFAULT CURRENT_TIMESTAMP")
    private Date creationDate;

    public Group() {

    }

    public Group(String id) {
        this.id = id;
    }

    public Group(String id, String name, String desc, Account owner) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.owner = owner;
    }

    public Group(String id, String name, String desc, Account owner, byte[] image) {
        this(id, name, desc, owner);
        this.image = image;
    }

    public Group(String id, String name, String desc, Account owner, byte[] image, Date creationDate) {
        this(id, name, desc, owner, image);
        this.creationDate = creationDate;
    }

    public String getBase64String() {
        return Base64.getEncoder().encodeToString(image);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public Account getOwner() {
        return owner;
    }

    public void setOwner(Account owner) {
        this.owner = owner;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public List<Account> getMembers() {
        return members;
    }

    public void setMembers(List<Account> members) {
        this.members = members;
    }


    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Group group = (Group) o;
        return id.equals(group.id) &&
                name.equals(group.name) &&
                Objects.equals(desc, group.desc) &&
                owner.equals(group.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, desc, owner);
    }
}

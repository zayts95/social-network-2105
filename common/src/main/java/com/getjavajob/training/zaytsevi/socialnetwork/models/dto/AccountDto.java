package com.getjavajob.training.zaytsevi.socialnetwork.models.dto;

import com.getjavajob.training.zaytsevi.socialnetwork.models.Account;

public class AccountDto {
    private String id;
    private String name;
    private String surname;

    public AccountDto(Account account) {
        this.id = account.getId();
        this.name = account.getName();
        this.surname = account.getSurname();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
}

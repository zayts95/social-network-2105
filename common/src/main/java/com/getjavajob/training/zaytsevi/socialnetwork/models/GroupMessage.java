package com.getjavajob.training.zaytsevi.socialnetwork.models;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "group_messages")
public class GroupMessage extends Message {

    @ManyToOne
    @JoinColumn(name = "to_smn")
    private Group to;

    public GroupMessage() {

    }

    public GroupMessage(Account from, Group to, String text, String imageUrl) {
        this.from = from;
        this.to = to;
        this.text = text;
        this.imageUrl = imageUrl;
    }

    public GroupMessage(String from, String to, String text, String imageUrl) {
        this.from = new Account(from);
        this.to = new Group(to);
        this.text = text;
        this.imageUrl = imageUrl;
    }

    public Group getTo() {
        return to;
    }

    public void setTo(Group to) {
        this.to = to;
    }
}

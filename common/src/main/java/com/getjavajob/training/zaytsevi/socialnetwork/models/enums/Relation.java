package com.getjavajob.training.zaytsevi.socialnetwork.models.enums;

public enum Relation {
    FRIEND,
    REQUEST_SENT,
    REQUESTED,
    NONE
}

package com.getjavajob.training.zaytsevi.socialnetwork.models.enums;

public enum Permission {
    BASIC("users:basic"),
    ADMIN("users:admin");

    private final String permission;

    Permission(String permission) {
        this.permission = permission;
    }

    public String getPermission() {
        return permission;
    }

}

package com.getjavajob.training.zaytsevi.socialnetwork.models.dto;

import com.getjavajob.training.zaytsevi.socialnetwork.models.Message;

import java.io.Serializable;
import java.util.Objects;

public class MessageDto implements Serializable {

    public static final long serialVersionUID = 2L;

    private String sender;
    private String text;
    private String imageUrl;
    private String date;

    public MessageDto() {

    }

    public MessageDto(Message message) {
        this.sender = message.getFrom().getId();
        this.text = message.getText();
        this.imageUrl = message.getImageUrl();
        if (message.getDate() != null) {
            this.date = message.getDateString();
        }
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MessageDto that = (MessageDto) o;
        return Objects.equals(sender, that.sender) && Objects.equals(text, that.text) && Objects.equals(imageUrl, that.imageUrl) && Objects.equals(date, that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sender, text, imageUrl, date);
    }
}

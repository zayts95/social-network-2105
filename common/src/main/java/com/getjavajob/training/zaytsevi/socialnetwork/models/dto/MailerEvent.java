package com.getjavajob.training.zaytsevi.socialnetwork.models.dto;

import com.getjavajob.training.zaytsevi.socialnetwork.models.Account;
import com.getjavajob.training.zaytsevi.socialnetwork.models.ProfileMessage;

import java.io.Serializable;

public class MailerEvent implements Serializable {

    public static final long serialVersionUID = 1L;

    private String id;
    private String sender;
    private String senderName;
    private String senderSurname;
    private String receiverEmail;
    private Type type;

    public MailerEvent(){

    }

    public MailerEvent(ProfileMessage message){
        this.sender = message.getFrom().getId();
        this.senderName = message.getFrom().getName();
        this.senderSurname = message.getFrom().getSurname();
        this.receiverEmail = message.getTo().getEmail();
        this.type = Type.MESSAGE;
    }

    public MailerEvent(Account from, Account to, Type type) {
        this.sender = from.getId();
        this.senderName = from.getName();
        this.senderSurname = from.getSurname();
        this.receiverEmail = to.getEmail();
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderSurname() {
        return senderSurname;
    }

    public void setSenderSurname(String senderSurname) {
        this.senderSurname = senderSurname;
    }

    public String getReceiverEmail() {
        return receiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public enum Type {
        MESSAGE,
        FRIEND_REQUEST
    }
}

package com.getjavajob.training.zaytsevi.socialnetwork.models;

import com.getjavajob.training.zaytsevi.socialnetwork.models.enums.Role;
import org.simpleframework.xml.Element;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "ACCOUNTS")
@Root
public class Account {

    @Enumerated(EnumType.STRING)
    private Role role;

    @Id
    @Element
    @NotBlank(message = "Id should not be empty")
    @Size(min = 2, max = 20, message = "Name should be between 2 and 30 characters")
    @Pattern(regexp = "^[A-Za-z0-9]+$", message = "A-Z and 0-9 only")
    private String id;

    @Size(min = 6, message = "Password should be between and 30 characters")
    private String password;

    @Element
    @NotBlank(message = "Name should not be empty")
    private String name;

    @Element(required = false)
    @Column(name = "middlename")
    private String middleName;

    @Element
    @NotBlank(message = "Surname should not be empty")
    private String surname;

    @Element(required = false)
    @Column(name = "birth")
    private Date birthDate;

    @Element
    @NotBlank(message = "Email should not be empty")
    @Email(message = "Email should be valid")
    private String email;

    @Element(required = false)
    private String instagram;

    @Element(required = false)
    @Column(name = "additional_info")
    private String additionalInfo;

    @OneToMany(mappedBy = "owner", fetch = FetchType.EAGER)
    @ElementList(required = false, type = Phone.class)
    private List<Phone> phones;

    @ManyToMany
    @JoinTable(
            name = "friends",
            joinColumns = @JoinColumn(name = "account"),
            inverseJoinColumns = @JoinColumn(name = "friend")
    )
    private List<Account> friends;

    @ManyToMany
    @JoinTable(
            name = "requests",
            joinColumns = @JoinColumn(name = "to_id"),
            inverseJoinColumns = @JoinColumn(name = "from_id")
    )
    private List<Account> friendRequests;

    @ManyToMany
    @JoinTable(
            name = "gr_acc",
            joinColumns = @JoinColumn(name = "acc"),
            inverseJoinColumns = @JoinColumn(name = "gr")
    )
    private List<Group> groups;
    @Column(name = "image_url")
    @Element
    private String imageUrl;

    public Account() {
        friends = new ArrayList<>();
        role = Role.USER;
    }

    public Account(String id) {
        this();
        this.id = id;
    }

    public Account(String id, String password, String name, String surname, String email) {
        this.id = id;
        this.password = password;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.phones = new ArrayList<>();
        role = Role.USER;
    }

    public Account(String id, String password, String name, String middleName, String surname, String imageUrl, String email, String instagram, String additionalInfo) {
        this(id, password, name, surname, email);
        this.middleName = middleName;
        this.instagram = instagram;
        this.additionalInfo = additionalInfo;
        this.imageUrl = imageUrl;
    }

    public Account(String id, String password, String name, String middleName, String surname, Date birthDate, String imageUrl, String email, String instagram, String additionalInfo) {
        this(id, password, name, middleName, surname, imageUrl, email, instagram, additionalInfo);
        this.birthDate = birthDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInstagram() {
        return instagram;
    }

    public void setInstagram(String instagram) {
        this.instagram = instagram;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<Account> getFriends() {
        return friends;
    }

    public void setFriends(List<Account> friends) {
        this.friends = friends;
    }

    public List<Group> getGroups() {
        return groups;
    }

    public void setGroups(List<Group> groups) {
        this.groups = groups;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public void addPhone(Phone phone) {
        if (phones == null) {
            phones = new ArrayList<Phone>();
        }
        phones.add(phone);
    }

    public List<Account> getFriendRequests() {
        return friendRequests;
    }

    public void setFriendRequests(List<Account> friendRequests) {
        this.friendRequests = friendRequests;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Account account = (Account) o;
        return Objects.equals(id, account.id) && Objects.equals(password, account.password) && Objects.equals(name, account.name) && Objects.equals(middleName, account.middleName) && Objects.equals(surname, account.surname) && Objects.equals(birthDate, account.birthDate) && Objects.equals(email, account.email) && Objects.equals(instagram, account.instagram) && Objects.equals(additionalInfo, account.additionalInfo) && Objects.equals(phones, account.phones) && Objects.equals(friends, account.friends) && Objects.equals(groups, account.groups) && Objects.equals(imageUrl, account.imageUrl);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, password, name, middleName, surname, birthDate, email, instagram, additionalInfo, phones, friends, groups, imageUrl);
    }
}

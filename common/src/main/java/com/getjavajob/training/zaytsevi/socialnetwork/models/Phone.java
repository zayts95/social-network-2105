package com.getjavajob.training.zaytsevi.socialnetwork.models;

import org.simpleframework.xml.Element;

import javax.persistence.*;

@Entity
@Table(name = "phones")
public class Phone {

    @Id
    @Column(name = "number")
    @Element
    private String number;
    @Element
    private String type;
    @ManyToOne
    @JoinColumn(name = "owner", nullable = false)
    private Account owner;

    public Phone() {

    }

    public Phone(String number, String type) {
        this.number = number;
        this.type = type;
    }

    public Phone(String number, String type, Account owner) {
        this(number, type);
        this.owner = owner;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Account getOwner() {
        return owner;
    }

    public void setOwner(Account owner) {
        this.owner = owner;
    }

    @Override
    public String toString() {
        if (type.equals("WORK")) {
            return "Work number: " + number;
        } else {
            return "Personal number: " + number;
        }
    }
}

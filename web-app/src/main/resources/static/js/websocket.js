let stompClient;

function connectToChat(userName) {
    console.log("connecting to chat...");
    let socket = new SockJS(`./chat`);
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        console.log("connected to: " + frame);
        stompClient.subscribe("/topic/messages/" + userName, function (response) {
            let data = JSON.parse(response.body);
            console.log(data);
            if (data.sender === selectedUser.accountId) {
                renderIncome(data.text, data.sender, getCurrentTime())
            }
        });
    });
}

function sendMsg(from, message, imageUrl) {
    stompClient.send("/app/chat/" + selectedUser.accountId, {}, JSON.stringify({
        sender: from,
        text: message,
        imageUrl: imageUrl
    }));
}

async function selectUser(accountId, name, surname, avatar) {
    $('.chat-history li').remove();
    await $.ajax({
        type: "GET",
        url: `./get30messages?accountId=${accountId}&offset=0`,
        dataType: 'JSON',
        success: await function (result) {
            var len = result.length;
            for (var i = 0; i < len; i++) {
                var message = {
                    sender: result[i].sender,
                    text: result[i].text,
                    imageUrl: result[i].imageUrl,
                    date: result[i].date
                };
                if (message.sender === globalId) {
                    renderOutcome(message.text, message.sender, message.date, message.imageUrl)
                } else {
                    renderIncome(message.text, message.sender, message.date, message.imageUrl)
                }
            }
        }
    });
    selectedUser = {
        accountId: accountId,
        name: name,
        surname: surname,
        avatar: avatar
    };
    console.log('user ' + accountId + 'selected');
    $('#avatar').attr("src", selectedUser.avatar);
    $('#selectedUser').attr("href", 'profile?accountId=' + selectedUser.accountId).html(selectedUser.name + ' ' + selectedUser.surname);
    scrollToBottom();
    setTimeout(function () {
        scrollToBottom();
    }.bind(this), 1000);
}

$(document).ready(function () {
    connectToChat(globalId);
    if (selectedUser != null) {
        selectUser(selectedUser.accountId, selectedUser.name, selectedUser.surname, selectedUser.avatar);
    }
});

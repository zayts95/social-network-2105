var count;
$(document).ready(function () {
    count = $('#count').val();
    var phones = phonesArray;
    deleteElements();
    addElements();

    $(document).on('click', '[id^=delete]', function (event) {
        event.preventDefault();
        let id = $(this).attr('id').substring(6);
        phones.splice(id - 1, 1);
        deleteElements();
        count--;
        if (count !== 0) {
            addElements();
        }
    });

    $('#add').click(function (event) {
        event.preventDefault();
        $('#validate-error').remove();
        let id = $(this).attr('id');
        let phone = {
            number: $('#newPhone').val(),
            type: $('#work').is(':checked') ? 'WORK' : "PERSONAL"
        };
        console.log(phone);
        console.log(phones);
        if (validatePhone(phone.number)) {
            phones.push(phone);
            deleteElements();
            count++;
            addElements();
        } else {
            $('.green-link').before('<p id="validate-error" style="color: red;">wrong phone format</p>');
        }
    });

    $('.green-link').click(function (event) {
        if (!confirm("Are you sure?")) {
            event.preventDefault();
        }
    });

    function addElements() {
        let html = "";
        for (let i = 1; i <= count; i++) {
            let type;
            if (phones[i - 1].type === 'WORK') {
                type = 'Work: '
            } else {
                type = 'Personal: '
            }
            html += '<input type="hidden" id="phone' + i + '" name="phone' + i + '" value="' + phones[i - 1].number + '">\n ' +
                '<input type="hidden" id="work' + i + '" name="work' + i + '" value="' + type + '">\n' +
                '<p id="p' + i + '" style="display: inline">' + phones[i - 1].number + '</p>\n' +
                '<button class="basic-link" id="delete' + i + '">delete</button> \n <br id=br' + i + '>\n'
        }
        $('#count').before(html);
    }

    function deleteElements() {
        for (let i = 1; i <= count; i++) {
            $('#phone' + i).remove();
            $('#work' + i).remove();
            $('#p' + i).remove();
            $('#delete' + i).remove();
            $('#br' + i).remove();
        }
    }

    function validatePhone(phone) {
        let regex = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/;
        return regex.test(phone);
    }

});
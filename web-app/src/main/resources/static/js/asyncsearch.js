$(document).ready(function () {
        $('#search').autocomplete({
            source: function (request, response) {
                $.post(`./findAccounts`, {search: request.term}, function (data) {
                    response($.map(data, function (account, i) {
                        return {value: account.id, label: account.name + " " + account.surname}
                    }));
                });
            },
            select: function (event, ui) {
                $(location).attr('href', `./profile?accountId=${ui.item.value}`);
            },
            minLength: 2
        });
    }
);
let $chatHistory;
let $button;
let $textarea;
let $chatHistoryList;

function init() {
    cacheDOM();
    bindEvents();
}

function bindEvents() {
    $button.on('click', addMessage.bind(this));
    $textarea.on('keyup', addMessageEnter.bind(this));
}

function cacheDOM() {
    $chatHistory = $('.chat-history');
    $button = $('#sendBtn');
    $textarea = $('#message-to-send');
    $chatHistoryList = $chatHistory.find('ul');
}

function renderIncome(message, userName, date, imageUrl) {
    // responses
    var templateResponse;
    if (imageUrl == null) {
        templateResponse = Handlebars.compile($("#message-response-template").html());
    } else {
        templateResponse = Handlebars.compile($("#message-response-template-image").html());
    }
    var contextResponse = {
        response: message,
        time: date,
        userName: userName,
        imageUrl: imageUrl
    };

    $chatHistoryList.append(templateResponse(contextResponse));
    scrollToBottom();
}

function renderOutcome(message, userName, date, imageUrl) {
    var templateResponse;
    if (imageUrl == null) {
        templateResponse = Handlebars.compile($("#message-template").html());
    } else {
        templateResponse = Handlebars.compile($("#message-template-image").html());
    }
    var contextResponse = {
        messageOutput: message,
        time: date,
        userName: userName,
        imageUrl: imageUrl
    };

    $chatHistoryList.append(templateResponse(contextResponse));
    scrollToBottom();
}

function sendMessage(message) {
    let username = $('#userName').val();
    console.log(username + '!!!!!')
    sendMsg(globalId, message);
    scrollToBottom();
    if (message.trim() !== '') {
        var template = Handlebars.compile($("#message-template").html());
        var context = {
            messageOutput: message,
            time: getCurrentTime(),
            toUserName: selectedUser
        };
        $chatHistoryList.append(template(context));
        scrollToBottom();
        $textarea.val('');
    }
}

function scrollToBottom() {
    $chatHistory.scrollTop($chatHistory[0].scrollHeight);
}

function getCurrentTime() {
    return new Date().toLocaleTimeString().replace(/([\d]+:[\d]{2})(:[\d]{2})(.*)/, "$1$3");
}

function addMessage() {
    sendMessage($textarea.val());
}

function addMessageEnter(event) {
    // enter was pressed
    if (event.keyCode === 13) {
        addMessage();
    }
}

init();


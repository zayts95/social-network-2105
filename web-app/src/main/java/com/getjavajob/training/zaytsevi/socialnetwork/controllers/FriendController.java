package com.getjavajob.training.zaytsevi.socialnetwork.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getjavajob.training.zaytsevi.socialnetwork.models.dto.MailerEvent;
import com.getjavajob.training.zaytsevi.socialnetwork.services.AccountService;
import com.getjavajob.training.zaytsevi.socialnetwork.services.FriendService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import javax.jms.Queue;

@Controller
public class FriendController {

    private static final Logger logger = LoggerFactory.getLogger(FriendController.class);

    public static final String REDIRECT_PROFILE = "redirect:/profile?accountId=";
    public static final String FRIENDS = "redirect:/friends?accountId=";

    private final FriendService friendService;
    private final AccountService accountService;
    private final JmsTemplate jmsTemplate;
    private final Queue queue;

    @Autowired
    public FriendController(FriendService friendService, AccountService accountService, JmsTemplate jmsTemplate, Queue queue) {
        this.friendService = friendService;
        this.accountService = accountService;
        this.jmsTemplate = jmsTemplate;
        this.queue = queue;
    }

    @PostMapping(value = "removeFriend")
    public String removeFriend(@SessionAttribute String globalId, @RequestParam String profileId) {
        friendService.removeFriend(globalId, profileId);
        return REDIRECT_PROFILE + profileId;
    }

    @PostMapping(value = "addFriend")
    public String addFriend(@SessionAttribute String globalId, @RequestParam String profileId) {
        friendService.addFriendRequest(globalId, profileId);
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            String jsonMessage = objectMapper.writeValueAsString(new MailerEvent(accountService.getAccountInfo(globalId),
                    accountService.getAccountInfo(profileId), MailerEvent.Type.FRIEND_REQUEST));
            jmsTemplate.convertAndSend(queue, jsonMessage);
        } catch (JsonProcessingException | JmsException e) {
            logger.warn("Unable to process an event to ActiveMQ");
        }
        return REDIRECT_PROFILE + profileId;
    }

    @PostMapping(value = "cancelRequest")
    public String cancelRequest(@SessionAttribute String globalId, @RequestParam String profileId) {
        friendService.cancelRequest(globalId, profileId);
        return REDIRECT_PROFILE + profileId;
    }

    @PostMapping(value = "acceptRequest")
    public String acceptRequest(@SessionAttribute String globalId, @RequestParam String profileId, @RequestParam String origin) {
        friendService.acceptFriendRequest(globalId, profileId);
        if (origin.equals("profile")) {
            return REDIRECT_PROFILE + profileId;
        } else {
            return FRIENDS + globalId;
        }
    }

    @PostMapping(value = "declineRequest")
    public String declineRequest(@SessionAttribute String globalId, @RequestParam String profileId, @RequestParam String origin) {
        friendService.declineFriendRequest(globalId, profileId);
        if (origin.equals("profile")) {
            return REDIRECT_PROFILE + profileId;
        } else {
            return FRIENDS + globalId;
        }
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleException(Exception exception) {
        return Utils.handleException(exception);
    }
}

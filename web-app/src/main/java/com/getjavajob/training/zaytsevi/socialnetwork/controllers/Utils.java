package com.getjavajob.training.zaytsevi.socialnetwork.controllers;

import com.getjavajob.training.zaytsevi.socialnetwork.models.Account;
import com.getjavajob.training.zaytsevi.socialnetwork.models.Group;
import org.springframework.lang.Nullable;
import org.springframework.web.servlet.ModelAndView;

import javax.imageio.ImageIO;
import javax.servlet.http.Part;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Utils {

    public static byte[] getImageBuffer(Part part) throws IOException {
        InputStream inputStream = part.getInputStream();
        BufferedImage image = ImageIO.read(inputStream);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", os);
        return os.toByteArray();
    }

    public static String getBirthString(Date birthDate) {
        String birthDateString;
        if (birthDate == null) {
            birthDateString = "";
        } else {
            birthDateString = birthDate.toString();
        }
        return birthDateString;
    }


    public static List<Group> get6RandomGroups(List<Group> groups) {
        List<Group> randomGroups = new ArrayList<>();
        if (groups.size() <= 6) {
            return groups;
        } else {
            Random random = new Random();
            int[] usedNumbers = new int[3];
            Arrays.fill(usedNumbers, -1);
            for (int i = 0; i <= 6; ) {
                int r = random.nextInt(groups.size());
                if (!contains(usedNumbers, r)) {
                    randomGroups.add(groups.get(r));
                    usedNumbers[i] = r;
                    i++;
                }
            }
        }
        return randomGroups;
    }

    public static List<Account> get3RandomAccounts(List<Account> accounts) {
        List<Account> randomFriends = new ArrayList<>();
        if (accounts.size() <= 3) {
            return accounts;
        } else {
            Random random = new Random();
            int[] usedNumbers = new int[3];
            Arrays.fill(usedNumbers, -1);
            for (int i = 0; i < 3; ) {
                int r = random.nextInt(accounts.size());
                if (!contains(usedNumbers, r)) {
                    randomFriends.add(accounts.get(r));
                    usedNumbers[i] = r;
                    i++;
                }
            }
        }
        return randomFriends;
    }


    public static ModelAndView createModelAndViewWithAttribute(String viewName, String attributeName, @Nullable Object attribute) {
        ModelAndView modelAndView = new ModelAndView(viewName);
        modelAndView.addObject(attributeName, attribute);
        return modelAndView;
    }

    public static ModelAndView handleException(Exception exception) {
        exception.printStackTrace();
        ModelAndView modelAndView = new ModelAndView("error");
        modelAndView.addObject("message", exception.getMessage());
        return modelAndView;
    }

    private static boolean contains(int[] array, int value) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) {
                return true;
            }
        }
        return false;
    }
}

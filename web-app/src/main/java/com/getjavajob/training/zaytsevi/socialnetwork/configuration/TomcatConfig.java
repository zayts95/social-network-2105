package com.getjavajob.training.zaytsevi.socialnetwork.configuration;

import org.apache.catalina.Context;
import org.apache.catalina.startup.Tomcat;
import org.apache.tomcat.util.descriptor.web.ContextResource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.embedded.tomcat.TomcatWebServer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class TomcatConfig {

    @Value("${tomcat.data-source-j-n-d-i}")
    String resourceName;
    @Value("${tomcat.driver-class-name}")
    String driverClassName;
    @Value("${tomcat.url}")
    String url;
    @Value("${tomcat.username}")
    String username;
    @Value("${tomcat.password}")
    String password;
    @Value("${tomcat.min-idle}")
    String minIdle;
    @Value("${tomcat.max-idle}")
    String maxIdle;
    @Value("${tomcat.max-wait}")
    String maxWaitMillis;

    @Bean
    public TomcatServletWebServerFactory tomcatFactory() {
        return new TomcatServletWebServerFactory() {
            @Override
            protected TomcatWebServer getTomcatWebServer(Tomcat tomcat) {
                tomcat.enableNaming();
                return super.getTomcatWebServer(tomcat);
            }

            @Override
            protected void postProcessContext(Context context) {
                ContextResource resource = new ContextResource();
                resource.setName(resourceName);
                resource.setType(DataSource.class.getName());
                resource.setProperty("driverClassName", driverClassName);
                resource.setProperty("url", url);
                resource.setProperty("factory", "org.apache.tomcat.jdbc.pool.DataSourceFactory");
                resource.setProperty("username", username);
                resource.setProperty("password", password);
                resource.setProperty("minIdle", minIdle);
                resource.setProperty("maxIdle", maxIdle);
                resource.setProperty("maxWaitMillis", maxWaitMillis);
                context.getNamingResources().addResource(resource);
            }
        };
    }
}

package com.getjavajob.training.zaytsevi.socialnetwork.controllers;


import com.getjavajob.training.zaytsevi.socialnetwork.models.Account;
import com.getjavajob.training.zaytsevi.socialnetwork.models.Group;
import com.getjavajob.training.zaytsevi.socialnetwork.models.GroupMessage;
import com.getjavajob.training.zaytsevi.socialnetwork.services.AccountService;
import com.getjavajob.training.zaytsevi.socialnetwork.services.GroupService;
import com.getjavajob.training.zaytsevi.socialnetwork.services.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Controller
@MultipartConfig
public class GroupController {

    private static final Logger logger = LoggerFactory.getLogger(GroupController.class);
    private static final String REDIRECT_CREATE_GROUP = "redirect:/creategroup";
    private static final String REDIRECT_GROUP = "redirect:/group?groupId=";
    private final AccountService accountService;
    private final GroupService groupService;
    private final MessageService messageService;

    @Autowired
    public GroupController(AccountService accountService, GroupService groupService, MessageService messageService) {
        this.accountService = accountService;
        this.groupService = groupService;
        this.messageService = messageService;
    }

    @PostMapping(value = "createGroupServlet")
    public ModelAndView postCreateGroup(@RequestParam String id, @RequestParam String name, @RequestParam String description, @RequestParam String owner) {
        if (id.isEmpty() || description.isEmpty() || name.isEmpty() || owner.isEmpty()) {
            ModelAndView modelAndView = new ModelAndView(REDIRECT_CREATE_GROUP);
            modelAndView.addObject("mes", "All fields must be filled");
            return modelAndView;
        } else {
            try {
                Group group = new Group(id, name, description, accountService.getAccountInfo(owner), null);
                groupService.addGroup(group);
                groupService.addMember(id, owner);
                return new ModelAndView(REDIRECT_GROUP + id);
            } catch (Exception exception) {
                exception.printStackTrace();
                logger.info("Exception: ", exception);
                ModelAndView modelAndView = new ModelAndView(REDIRECT_CREATE_GROUP);
                modelAndView.addObject("mes", exception.getMessage());
                return modelAndView;
            }
        }
    }

    @PostMapping(value = "editGroupPost")
    public ModelAndView postEditGroup(@RequestParam String id, @RequestParam String name,
                                      @RequestParam String description, @RequestParam String owner) {
        if (id.isEmpty() || name.isEmpty() || description.isEmpty()) {
            ModelAndView modelAndView = new ModelAndView("redirect:/editgroup");
            modelAndView.addObject("mes", "All fields must be filled");
            return modelAndView;
        } else {
            try {
                Group group = new Group(id, name, description, accountService.getAccountInfo(owner));
                logger.debug("Updating group {}", id);
                groupService.updateGroup(group);
                return new ModelAndView(REDIRECT_GROUP + id);
            } catch (Exception exception) {
                logger.info("Exception: ", exception);
                exception.printStackTrace();
                return Utils.createModelAndViewWithAttribute(REDIRECT_CREATE_GROUP, "mes", exception.getMessage());
            }
        }
    }

    @GetMapping(value = "editgroup")
    public ModelAndView editGroup(@SessionAttribute String globalId, @RequestParam String groupId) {
        Group group = groupService.getGroupById(groupId);
        if (!group.getOwner().getId().equals(globalId)) {
            return new ModelAndView(REDIRECT_GROUP + groupId);
        }
        ModelAndView modelAndView = new ModelAndView("editgroup");
        modelAndView.addObject("id", group.getId());
        modelAndView.addObject("name", group.getName());
        modelAndView.addObject("description", group.getDesc());
        return modelAndView;
    }

    @PostMapping(value = "groupLogoUploadServlet")
    public ModelAndView postGroupLogoUpload(HttpServletRequest req) throws IOException, ServletException {
        Part part = req.getPart("file");
        String id = req.getParameter("groupId");
        InputStream inputStream = null;
        if (part != null) {
            inputStream = part.getInputStream();
        }
        groupService.updateImage(id, inputStream);
        return new ModelAndView(REDIRECT_GROUP + id);
    }


    @PostMapping(value = "groupsearch")
    public ModelAndView groupSearch(@RequestParam String search, @RequestParam("page") int pg) {
        ModelAndView modelAndView = new ModelAndView("groupsearch");
        modelAndView.addObject("search", search);
        search = "%" + search + "%";
        modelAndView.addObject("page", pg);
        List<Group> results = groupService.find10Groups(search, pg);
        modelAndView.addObject("results", results);
        modelAndView.addObject("size", results.size());
        return modelAndView;
    }

    @GetMapping(value = "group")
    public ModelAndView group(@RequestParam String groupId, @SessionAttribute String globalId) {
        Group group = groupService.getGroupById(groupId);
        ModelAndView modelAndView = new ModelAndView("group");
        modelAndView.addObject("group", group);
        boolean member = groupService.checkMember(globalId, groupId);
        modelAndView.addObject("member", member);
        modelAndView.addObject("memberCount", group.getMembers().size());
        List<Account> members = Utils.get3RandomAccounts(group.getMembers());
        modelAndView.addObject("members", members);
        List<GroupMessage> messages = messageService.getAllMessagesToGroup(group);
        modelAndView.addObject("messages", messages);
        modelAndView.addObject("messageCount", messages.size());
        return modelAndView;
    }

    @GetMapping(value = "groups")
    public ModelAndView groups(@RequestParam String accountId) {
        Account account = accountService.getAccountById(accountId);
        ModelAndView modelAndView = new ModelAndView("groups");
        modelAndView.addObject("groups", account.getGroups());
        return modelAndView;
    }

    @PostMapping(value = "joinGroupServlet")
    public String joinGroup(@SessionAttribute String globalId, @RequestParam String groupId) {
        groupService.addMember(groupId, globalId);
        logger.debug("{} joins group {}", globalId, groupId);
        return REDIRECT_GROUP + groupId;
    }

    @PostMapping(value = "leaveGroupServlet")
    public String leaveGroup(@SessionAttribute String globalId, @RequestParam String groupId) {
        groupService.removeMember(groupId, globalId);
        logger.debug("{} leaves group {}", globalId, groupId);
        return REDIRECT_GROUP + groupId;
    }

    @GetMapping(value = "members")
    public ModelAndView members(@RequestParam String groupId) {
        Group group = groupService.getGroupById(groupId);
        ModelAndView modelAndView = new ModelAndView("members");
        modelAndView.addObject("members", group.getMembers());
        return modelAndView;
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleException(Exception exception) {
        return Utils.handleException(exception);
    }
}

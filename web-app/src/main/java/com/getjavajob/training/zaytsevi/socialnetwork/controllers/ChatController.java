package com.getjavajob.training.zaytsevi.socialnetwork.controllers;

import com.getjavajob.training.zaytsevi.socialnetwork.models.Account;
import com.getjavajob.training.zaytsevi.socialnetwork.models.PersonalMessage;
import com.getjavajob.training.zaytsevi.socialnetwork.models.dto.MessageDto;
import com.getjavajob.training.zaytsevi.socialnetwork.services.AccountService;
import com.getjavajob.training.zaytsevi.socialnetwork.services.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@RestController
@CrossOrigin
public class ChatController {

    private static final Logger logger = LoggerFactory.getLogger(ChatController.class);

    private final SimpMessagingTemplate simpMessagingTemplate;
    private final AccountService accountService;
    private final MessageService messageService;

    @Autowired
    public ChatController(SimpMessagingTemplate simpMessagingTemplate, AccountService accountService, MessageService messageService) {
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.accountService = accountService;
        this.messageService = messageService;
    }

    @MessageMapping("/chat/{to}")
    public void sendMessage(@DestinationVariable String to, MessageDto message) {
        logger.debug("handling send message {} to {}", message.getText(), to);
        Account accountTo = new Account(to);
        Account accountFrom = new Account(message.getSender());
        PersonalMessage personalMessage = new PersonalMessage(accountFrom, accountTo, message.getText(), null);
        messageService.createNewMessage(personalMessage);
        simpMessagingTemplate.convertAndSend("/topic/messages/" + to, message);
    }

    @GetMapping("/messenger")
    public ModelAndView chat(@SessionAttribute String globalId) {
        ModelAndView modelAndView = new ModelAndView("messenger");
        List<Account> dialogs = accountService.getDialogs(globalId);
        Account first = null;
        if (!dialogs.isEmpty()) {
            first = dialogs.get(0);
        }
        modelAndView.addObject("dialogs", dialogs);
        modelAndView.addObject("empty", dialogs.isEmpty());
        if (first != null) {
            modelAndView.addObject("id", first.getId());
            modelAndView.addObject("name", first.getName());
            modelAndView.addObject("surname", first.getSurname());
            modelAndView.addObject("imageUrl", first.getImageUrl());
        }
        return modelAndView;
    }

    @GetMapping("/get30messages")
    public List<MessageDto> get30messages(@RequestParam String accountId, @SessionAttribute String globalId, @RequestParam int offset) {
        return messageService.get30Messages(globalId, accountId, offset);
    }


    @PostMapping("/uploadimage")
    public String uploadImage(HttpServletRequest req, @SessionAttribute String globalId) {
        String result = "";
        try {
            if (req.getPart("image").getSize() > 0) {
                Part part = req.getPart("image");
                byte[] buffer = Utils.getImageBuffer(part);
                InputStream is = new ByteArrayInputStream(buffer);
                result = messageService.uploadImage(globalId, is, buffer.length);
            }
        } catch (IOException | ServletException e) {
            logger.info("Exception ", e);
        }
        return result;
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleException(Exception exception) {
        return Utils.handleException(exception);
    }
}

package com.getjavajob.training.zaytsevi.socialnetwork.controllers;

import com.getjavajob.training.zaytsevi.socialnetwork.models.Account;
import com.getjavajob.training.zaytsevi.socialnetwork.models.Group;
import com.getjavajob.training.zaytsevi.socialnetwork.models.Phone;
import com.getjavajob.training.zaytsevi.socialnetwork.models.ProfileMessage;
import com.getjavajob.training.zaytsevi.socialnetwork.models.dto.AccountDto;
import com.getjavajob.training.zaytsevi.socialnetwork.services.AccountService;
import com.getjavajob.training.zaytsevi.socialnetwork.services.FriendService;
import com.getjavajob.training.zaytsevi.socialnetwork.services.MessageService;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.validation.Valid;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
public class ProfileController {
    public static final String REDIRECT_LOGIN = "redirect:/login";
    public static final String REDIRECT_MY_PROFILE = "redirect:/myprofile";
    public static final String REDIRECT_EDIT = "redirect:/edit";
    public static final String BASIC_IMAGE_URL = "https://zayts95sn.s3.eu-north-1.amazonaws.com/image.png";
    private static final Logger logger = LoggerFactory.getLogger(ProfileController.class);
    private final AccountService accountService;
    private final MessageService messageService;
    private final FriendService friendService;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public ProfileController(AccountService accountService, MessageService messageService, FriendService friendService, PasswordEncoder passwordEncoder) {
        this.accountService = accountService;
        this.messageService = messageService;
        this.friendService = friendService;
        this.passwordEncoder = passwordEncoder;
    }

    private static void downloadFileProperties(HttpServletResponse resp, File downloadFile) {
        try (FileInputStream inputStream = new FileInputStream(downloadFile)) {
            String mimeType = "application/octet-stream";
            resp.setContentType(mimeType);
            resp.setContentLength((int) downloadFile.length());
            String headerKey = "Content-Disposition";
            String headerValue = String.format("attachment; filename=\"%s\"", downloadFile.getName());
            resp.setHeader(headerKey, headerValue);
            OutputStream outStream = resp.getOutputStream();
            byte[] buffer = new byte[4067];
            int bytesRead = -1;
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                outStream.write(buffer, 0, bytesRead);
            }
            outStream.close();
        } catch (IOException ioExObj) {
            ioExObj.printStackTrace();
        }
    }

    @GetMapping(value = {"myprofile", ""})
    public ModelAndView myProfile(@SessionAttribute(required = false) String globalId) {
        if (globalId == null) {
            logger.debug("Redirect to login");
            return new ModelAndView(REDIRECT_LOGIN);
        }
        ModelAndView modelAndView = new ModelAndView("myprofile");
        Account account = accountService.getAccountById(globalId);
        return setProfileModelAndView(account, modelAndView);
    }

    @GetMapping(value = "profile")
    public ModelAndView profile(@RequestParam("accountId") String accountId, @SessionAttribute("globalId") String globalId) {
        if (globalId.equals(accountId)) {
            return new ModelAndView(REDIRECT_MY_PROFILE);
        }
        Account account = accountService.getAccountById(accountId);
        ModelAndView modelAndView = new ModelAndView("profile");
        modelAndView.addObject("relation", friendService.relation(globalId, accountId));
        return setProfileModelAndView(account, modelAndView);
    }

    @GetMapping(value = "edit")
    public ModelAndView edit(@SessionAttribute("globalId") String globalId) {
        Account account = accountService.getAccountInfo(globalId);
        ModelAndView modelAndView = new ModelAndView("edit");
        modelAndView.addObject("account", account);
        modelAndView.addObject("phonesSize", account.getPhones().size());
        modelAndView.addObject("phones", account.getPhones());
        Date birthDate = account.getBirthDate();
        String birthDateString = Utils.getBirthString(birthDate);
        modelAndView.addObject("birth", birthDateString);
        return modelAndView;
    }

    @PostMapping(value = "edit")
    public String editPost(@SessionAttribute("globalId") String globalId, @ModelAttribute("account") @Valid Account account, BindingResult bindingResult, @RequestParam String birthString,
                           @RequestParam Map<String, String> allParams) {
        if (bindingResult.hasErrors()) {
            return "edit";
        }
        if (!birthString.equals("")) {
            Date birthDate = Date.valueOf(birthString);
            account.setBirthDate(birthDate);
        }
        setAccountsPhones(account, allParams);
        accountService.updateAccount(account);
        logger.info("Account updated: {}", globalId);
        return REDIRECT_MY_PROFILE;
    }

    @GetMapping("/login")
    public ModelAndView getLoginPage() {
        logger.debug("login page");
        return new ModelAndView("login");
    }

    @GetMapping(value = "friends")
    public ModelAndView friends(@RequestParam String accountId) {
        Account account = accountService.getAccountWithFriends(accountId);
        ModelAndView modelAndView = new ModelAndView("friends");
        modelAndView.addObject("friends", account.getFriends());
        modelAndView.addObject("requests", account.getFriendRequests());
        return modelAndView;
    }

    @PostMapping(value = "imageUpload", consumes = "multipart/form-data")
    public ModelAndView postImageUpload(HttpServletRequest req, @SessionAttribute String globalId) throws IOException, ServletException {
        Part part = req.getPart("avatar");
        byte[] buffer = Utils.getImageBuffer(part);
        InputStream is = new ByteArrayInputStream(buffer);
        accountService.uploadAvatar(globalId, globalId, is, buffer.length);
        logger.info("image uploaded for {}", globalId);
        return new ModelAndView(REDIRECT_MY_PROFILE);
    }

    @PostMapping(value = "registration")
    public String register(@ModelAttribute("account") @Valid Account account, BindingResult bindingResult, @RequestParam String birthString) {
        if (bindingResult.hasErrors()) {
            return "register";
        }
        account.setId(account.getId().toLowerCase());
        account.setImageUrl(BASIC_IMAGE_URL);
        if (!birthString.equals("")) {
            Date birthDate = Date.valueOf(birthString);
            account.setBirthDate(birthDate);
        }
        account.setPassword(passwordEncoder.encode(account.getPassword()));
        accountService.createNewAccount(account);
        return REDIRECT_LOGIN;
    }

    @PostMapping(value = "search")
    public ModelAndView search(@RequestParam String search, @RequestParam("page") int pg) {
        ModelAndView modelAndView = new ModelAndView("search");
        modelAndView.addObject("search");
        search = "%" + search + "%";
        modelAndView.addObject("page", pg);
        List<Account> results = accountService.find10Accounts(search, pg);
        modelAndView.addObject("results", results);
        modelAndView.addObject("size", results.size());
        return modelAndView;
    }

    @PostMapping(value = "findAccounts")
    @ResponseBody
    public List<AccountDto> findAccounts(@RequestParam String search) {
        search = "%" + search + "%";
        return accountService.asyncSearch(search);
    }

    @GetMapping(value = "downloadxml")
    public void downloadXml(@SessionAttribute String globalId, HttpServletResponse httpServletResponse) {
        Serializer serializer = new Persister();
        Account account = accountService.getAccountInfo(globalId);
        List<Phone> phonesArrayList = new ArrayList<>(account.getPhones());
        account.setPhones(phonesArrayList);
        File result = new File(globalId + ".xml");
        try {
            serializer.write(account, result);
        } catch (Exception exception) {
            exception.printStackTrace();
            logger.info("Exception: ", exception);
        }
        downloadFileProperties(httpServletResponse, result);
    }

    @PostMapping(value = "/uploadXml")
    public ModelAndView editAccountFromXml(@RequestParam("file") MultipartFile multipartFile) {
        try {
            Serializer serializer = new Persister();
            String content = new String(multipartFile.getBytes(), StandardCharsets.UTF_8);
            Account account = serializer.read(Account.class, content);
            List<Phone> phones = account.getPhones();
            for (Phone p : phones) {
                p.setOwner(account);
            }
            accountService.updateAccount(account);
            return new ModelAndView(REDIRECT_MY_PROFILE);
        } catch (Exception exception) {
            exception.printStackTrace();
            logger.info("Exception: ", exception);
            ModelAndView modelAndView = new ModelAndView(REDIRECT_EDIT);
            modelAndView.addObject("mes", "unable to read data");
            return modelAndView;
        }
    }

    @PostMapping("/remove")
    @PreAuthorize("hasAuthority('users:admin')")
    public String removeAccount(@RequestParam("profileId") String profileId) {
        accountService.removeAccount(profileId);
        return REDIRECT_MY_PROFILE;
    }

    @GetMapping("/registration")
    public ModelAndView registration() {
        ModelAndView modelAndView = new ModelAndView("register");
        modelAndView.addObject("account", new Account());
        return modelAndView;
    }

    @GetMapping("/error")
    public ModelAndView error() {
        return new ModelAndView("error");
    }

    @GetMapping("/creategroup")
    public ModelAndView createGroup() {
        return new ModelAndView("creategroup");
    }

    @PostMapping("/changePassword")
    public ModelAndView changePassword(@RequestParam String oldPassword, @RequestParam String newPassword, @SessionAttribute String globalId) {
        Account account = accountService.getAccountById(globalId);
        if (account.getPassword().equals(passwordEncoder.encode(oldPassword))) {
            account.setPassword(passwordEncoder.encode(newPassword));
            accountService.mergeAccount(account);
            return new ModelAndView(REDIRECT_MY_PROFILE);
        } else {
            ModelAndView modelAndView = new ModelAndView(REDIRECT_EDIT);
            modelAndView.addObject("pasmes", "Old password doesn't match");
            return modelAndView;
        }
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleException(Exception exception) {
        return Utils.handleException(exception);
    }

    private ModelAndView setProfileModelAndView(Account account, ModelAndView modelAndView) {
        modelAndView.addObject("account", account);
        modelAndView.addObject("birth", getBirthString(account.getBirthDate()));
        modelAndView.addObject("friendsCount", account.getFriends().size());
        List<Account> friends = Utils.get3RandomAccounts(account.getFriends());
        modelAndView.addObject("friends", friends);
        modelAndView.addObject("groupsCount", account.getGroups().size());
        List<Group> groups = Utils.get6RandomGroups(account.getGroups());
        modelAndView.addObject("groups", groups);
        List<ProfileMessage> messages = messageService.getAllMessagesToSmn(account);
        int messageCount = messages.size();
        modelAndView.addObject("messageCount", messageCount);
        modelAndView.addObject("messages", messages);
        return modelAndView;
    }

    private String getBirthString(Date birthDate) {
        String birthDateString;
        if (birthDate == null) {
            birthDateString = "";
        } else {
            birthDateString = birthDate.toString();
        }
        return birthDateString;
    }

    private void setAccountsPhones(Account account, Map<String, String> params) {
        int count = 1;
        while (true) {
            String number = params.get("phone" + count);
            if (number == null) {
                break;
            }
            String work = params.get("work" + count).equals("true") ? "WORK" : "PERSONAL";
            account.addPhone(new Phone(number, work, account));
            count++;
        }
    }
}

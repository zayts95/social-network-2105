package com.getjavajob.training.zaytsevi.socialnetwork.security;

import com.getjavajob.training.zaytsevi.socialnetwork.models.Account;
import com.getjavajob.training.zaytsevi.socialnetwork.services.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CookieAuthenticationSuccessHandlerImpl extends SavedRequestAwareAuthenticationSuccessHandler {

    final AccountService accountService;

    @Autowired
    public CookieAuthenticationSuccessHandlerImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        String email = ((UserDetails) authentication.getPrincipal()).getUsername();
        Account account = accountService.getAccountByEmail(email);
        httpServletRequest.getSession().setAttribute("globalId", account.getId());
        super.onAuthenticationSuccess(httpServletRequest, httpServletResponse, authentication);
    }
}
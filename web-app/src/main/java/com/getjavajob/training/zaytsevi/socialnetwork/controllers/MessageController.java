package com.getjavajob.training.zaytsevi.socialnetwork.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getjavajob.training.zaytsevi.socialnetwork.models.*;
import com.getjavajob.training.zaytsevi.socialnetwork.models.dto.MailerEvent;
import com.getjavajob.training.zaytsevi.socialnetwork.models.dto.MessageDto;
import com.getjavajob.training.zaytsevi.socialnetwork.services.AccountService;
import com.getjavajob.training.zaytsevi.socialnetwork.services.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.jms.Queue;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;
import java.io.IOException;
import java.util.List;

@Controller
public class MessageController {

    private static final Logger logger = LoggerFactory.getLogger(MessageController.class);

    private final AccountService accountService;
    private final MessageService messageService;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final JmsTemplate jmsTemplate;
    private final Queue queue;

    @Autowired
    public MessageController(AccountService accountService,
                             MessageService messageService, SimpMessagingTemplate simpMessagingTemplate, JmsTemplate jmsTemplate, Queue queue) {
        this.accountService = accountService;
        this.messageService = messageService;
        this.simpMessagingTemplate = simpMessagingTemplate;
        this.jmsTemplate = jmsTemplate;
        this.queue = queue;
    }

    @GetMapping(value = "message")
    public ModelAndView message(@SessionAttribute String globalId, @RequestParam long id) {
        PersonalMessage message = messageService.getMessageById(id);
        ModelAndView modelAndView = new ModelAndView("message");
        modelAndView.addObject("message", message);
        if (!message.getFrom().getId().equals(globalId) && !message.getTo().getId().equals(globalId)) {
            return new ModelAndView("redirect:/mymessages?offset=1");
        }
        return modelAndView;
    }

    @GetMapping(value = "mymessages")
    public ModelAndView myMessages(@RequestParam int offset, @SessionAttribute String globalId) {
        ModelAndView modelAndView = new ModelAndView("mymessages");
        modelAndView.addObject("offset", offset);
        List<PersonalMessage> messages = messageService.get10MessagesToSmn(globalId, offset);
        modelAndView.addObject("messages", messages);
        int count = messageService.inboxCount(globalId);
        modelAndView.addObject("count", count);
        int begin = offset * 10 - 9;
        modelAndView.addObject("begin", begin);
        int end = offset * 10;
        modelAndView.addObject("end", end);
        return modelAndView;
    }

    @PostMapping(value = "removeMessageServlet")
    public String removeMessage(@RequestParam long messageId, @RequestParam String destination, @RequestParam String origin) {
        if (destination.equals("profile")) {
            messageService.removeMessageById(messageId);
        } else if (destination.equals("group")) {
            messageService.removeGroupMessageById(messageId);
        }
        return "redirect:/" + origin;
    }

    @PostMapping(value = "sendMessageServlet")
    public ModelAndView postSendMessage(HttpServletRequest req, @SessionAttribute String globalId, @RequestPart Part image) throws IOException {
        String text = req.getParameter("text");
        String destination = req.getParameter("destination");
        String origin = req.getParameter("origin");
        if (destination.equals("group")) {
            Message message = new GroupMessage(globalId, req.getParameter("groupId"),
                    text, messageService.uploadImageFromPart(globalId, image));
            messageService.createNewMessage(message);
        } else if (destination.equals("profile")) {
            ProfileMessage message = new ProfileMessage(accountService.getAccountInfo(globalId),
                    accountService.getAccountInfo(req.getParameter("accountId")),
                    text, messageService.uploadImageFromPart(globalId, image));
            messageService.createNewMessage(message);
            ObjectMapper objectMapper = new ObjectMapper();
            try {
                String jsonMessage = objectMapper.writeValueAsString(new MailerEvent(message));
                jmsTemplate.convertAndSend(queue, jsonMessage);
            } catch (JsonProcessingException | JmsException e) {
                logger.warn("Unable to process an event to ActiveMQ");
            }
        } else {
            PersonalMessage message = new PersonalMessage(globalId, req.getParameter("accountId"),
                    text, messageService.uploadImageFromPart(globalId, image));
            messageService.createNewMessage(message);
            simpMessagingTemplate.convertAndSend("/topic/messages/" + (message).getTo().getId(), new MessageDto(message));
        }
        return new ModelAndView("redirect:/" + origin);
    }

    @GetMapping(value = "sendmessage")
    public ModelAndView sendMessage(@RequestParam String accountId) {
        Account account = accountService.getAccountById(accountId);
        ModelAndView modelAndView = new ModelAndView("sendmessage");
        modelAndView.addObject("account", account);
        return modelAndView;
    }

    @GetMapping(value = "sentmessages")
    public ModelAndView sentMessages(@RequestParam int offset, @SessionAttribute String globalId) {
        ModelAndView modelAndView = new ModelAndView("sentmessages");
        modelAndView.addObject("offset", offset);
        List<PersonalMessage> messages = messageService.get10MessagesFromSmn(globalId, offset);
        modelAndView.addObject("messages", messages);
        int count = messageService.sentCount(globalId);
        modelAndView.addObject("count", count);
        int begin = offset * 10 - 9;
        modelAndView.addObject("begin", begin);
        int end = offset * 10;
        modelAndView.addObject("end", end);
        return modelAndView;
    }

    @ExceptionHandler(Exception.class)
    public ModelAndView handleException(Exception exception) {
        return Utils.handleException(exception);
    }
}
FROM openjdk:8
ADD web-app/target/web-app-1.0-SNAPSHOT.jar web-app-1.0-SNAPSHOT.jar
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "web-app-1.0-SNAPSHOT.jar"]
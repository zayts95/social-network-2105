package com.getjavajob.training.zaytsevi.socialnetwork.repositories;

import com.getjavajob.training.zaytsevi.socialnetwork.models.Group;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupRepository extends CrudRepository<Group, String> {

    @Query(nativeQuery = true, value = "select distinct * from (select * from groups where lower(group_id) like lower(:search) and lower(group_name) like lower(:search)) a offset :offset limit 10")
    List<Group> find10Groups(@Param("search") String search, @Param("offset") int offset);

    @Query(nativeQuery = true, value = "insert into gr_acc (gr, acc) values(:group, :account)")
    @Modifying
    void addMember(@Param("group") String groupId, @Param("account") String accountId);

    @Query(nativeQuery = true, value = "delete from gr_acc where gr= :group and acc= :account")
    @Modifying
    void removeMember(@Param("group") String groupId, @Param("account") String accountId);

    @Query(nativeQuery = true, value = "select case when count(*) > 0 then true else false end from gr_acc where acc=:account and gr=:group")
    boolean member(@Param("account") String accountId, @Param("group") String groupId);
}

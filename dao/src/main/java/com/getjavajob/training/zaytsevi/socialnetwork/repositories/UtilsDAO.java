package com.getjavajob.training.zaytsevi.socialnetwork.repositories;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.math.BigInteger;

public class UtilsDAO {
    public static boolean checkExistence(EntityManager entityManager, String sql, String firstParameter, String secondParameter) {
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter(1, firstParameter);
        query.setParameter(2, secondParameter);
        BigInteger object = (BigInteger) query.getSingleResult();
        int i = object == null ? 0 : object.intValue();
        return i > 0;
    }
}

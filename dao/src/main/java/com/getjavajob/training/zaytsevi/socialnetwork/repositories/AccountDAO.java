package com.getjavajob.training.zaytsevi.socialnetwork.repositories;

import com.getjavajob.training.zaytsevi.socialnetwork.models.Account;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class AccountDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public void removeAccount(String accountId) {
        Account account = entityManager.find(Account.class, accountId);
        entityManager.remove(account);
    }

    public void updateImageUrl(String id, String url) {
        Account account = entityManager.find(Account.class, id);
        account.setImageUrl(url);
        entityManager.merge(account);
    }

    public Account getAccountByEmail(String email) {
        String jpql = "Select a from Account a where a.email = ?1";
        Query query = entityManager.createQuery(jpql);
        query.setParameter(1, email);
        return (Account) query.getSingleResult();
    }

    @SuppressWarnings("unchecked")
    public List<Account> find10Accounts(String search, int offset) {
        String sql = "Select distinct a from Account a WHERE LOWER(CONCAT(a.name, ' ', a.surname)) like ?1 or LOWER(a.id) LIKE ?2";
        Query query = entityManager.createQuery(sql);
        query.setParameter(1, search);
        query.setParameter(2, search);
        query.setMaxResults(10);
        query.setFirstResult(offset);
        return query.getResultList();
    }

    @SuppressWarnings("unchecked")
    public List<Account> getDialogs(String globalId) {
        String sql = "select distinct * from accounts where id in (select distinct from_smn from personal_messages where to_smn = :globalId union select distinct to_smn from personal_messages where from_smn = :globalId) or id in (select distinct friend from friends where account = :globalId)";
        Query query = entityManager.createNativeQuery(sql, Account.class);
        query.setParameter("globalId", globalId);
        return query.getResultList();
    }

    public String checkLogin(String email, String password) {
        String sql = "SELECT a.id FROM Account a WHERE a.email = '" + email + "' and a.password = '" + password + "'";
        Query query = entityManager.createQuery(sql);
        return (String) query.getSingleResult();
    }

    public void addAccount(Account account) {
        entityManager.persist(account);
    }


    public Account getAccountById(String id) {
        return entityManager.find(Account.class, id);
    }

    public void updateAccount(Account newAccount, Account entityAccount) {
        entityAccount.setName(newAccount.getName());
        entityAccount.setMiddleName(newAccount.getMiddleName());
        entityAccount.setSurname(newAccount.getSurname());
        entityAccount.setBirthDate(newAccount.getBirthDate());
        entityAccount.setEmail(newAccount.getEmail());
        entityAccount.setInstagram(newAccount.getInstagram());
        entityAccount.setAdditionalInfo(newAccount.getAdditionalInfo());
        entityManager.merge(entityAccount);
    }

    public void mergeAccount(Account account) {
        entityManager.merge(account);
    }
}
package com.getjavajob.training.zaytsevi.socialnetwork.repositories;

import com.getjavajob.training.zaytsevi.socialnetwork.models.Phone;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class PhoneDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public void addPhones(List<Phone> phones) {
        for (Phone p : phones) {
            entityManager.persist(p);
        }
    }

    public void removePhones(List<Phone> phones) {
        for (Phone p : phones) {
            entityManager.remove(p);
        }
    }
}

package com.getjavajob.training.zaytsevi.socialnetwork.repositories;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.ObjectMetadata;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.io.InputStream;

@Repository
public class AwsDAO {
    private final String bucket;
    private final AmazonS3Client s3;

    @Autowired
    public AwsDAO(@Value("${aws.sq.accessKeyID}") String accessKeyId,
                  @Value("${aws.sq.secretAccessKey}") String secretAccessKey,
                  @Value("${aws.sq.region}") String region,
                  @Value("${aws.sq.bucket}") String bucket) {
            this.bucket = bucket;
            BasicAWSCredentials basicAWSCredentials = new BasicAWSCredentials(accessKeyId, secretAccessKey);
            AWSCredentialsProvider awsStaticCredentialsProvider = new AWSStaticCredentialsProvider(basicAWSCredentials);
            s3 = (AmazonS3Client) AmazonS3ClientBuilder.standard().withCredentials(awsStaticCredentialsProvider).withRegion(region).build();
    }

    public String uploadImage(String key, InputStream inputStream, int contentLength) throws SdkClientException {
        return uploadImage(key, inputStream, contentLength, this.bucket);
    }

    public String uploadImage(String key, InputStream inputStream, int contentLength, String bucket) throws SdkClientException {
        ObjectMetadata objectMetadata = new ObjectMetadata();
        objectMetadata.setContentLength(contentLength);
        objectMetadata.setContentType("image/jpeg");
        s3.putObject(bucket, key, inputStream, objectMetadata);
        return s3.getResourceUrl(bucket, key);
    }
}

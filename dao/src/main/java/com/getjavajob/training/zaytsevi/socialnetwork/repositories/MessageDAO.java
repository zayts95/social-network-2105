package com.getjavajob.training.zaytsevi.socialnetwork.repositories;

import com.getjavajob.training.zaytsevi.socialnetwork.models.*;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Repository
public class MessageDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public void createNewMessage(Message message) {
        entityManager.persist(message);
    }

    public List<PersonalMessage> get10MessagesFromSmn(String from, int offset) {
        String jpql = "Select m from PersonalMessage m where m.from = ?1 order by m.date desc";
        return getPersonalMessagesWithOneParameter(from, offset, jpql);
    }

    public List<PersonalMessage> get10MessagesToSmn(String to, int offset) {
        String jpql = "Select m from PersonalMessage m where m.to = ?1 order by m.date desc";
        return getPersonalMessagesWithOneParameter(to, offset, jpql);
    }

    private List<PersonalMessage> getPersonalMessagesWithOneParameter(String to, int offset, String jpql) {
        Query query = entityManager.createQuery(jpql);
        Account account = new Account();
        account.setId(to);
        query.setParameter(1, account);
        query.setMaxResults(10);
        query.setFirstResult(offset);
        return query.getResultList();
    }

    public List<ProfileMessage> getAllMessagesToSmn(Account to) {
        String jpql = "select m from ProfileMessage m where m.to = ?1 order by m.date desc";
        Query query = entityManager.createQuery(jpql);
        query.setParameter(1, to);
        return query.getResultList();
    }

    public List<GroupMessage> getAllMessagesToGroup(Group to) {
        String jpql = "select m from GroupMessage m where m.to = ?1 order by m.date desc";
        Query query = entityManager.createQuery(jpql);
        query.setParameter(1, to);
        return query.getResultList();
    }

    public PersonalMessage getMessageById(long id) {
        return entityManager.find(PersonalMessage.class, id);
    }

    public void removeMessageById(long id) {
        ProfileMessage message = entityManager.find(ProfileMessage.class, id);
        entityManager.remove(message);
    }

    public void removeGroupMessageById(long id) {
        GroupMessage message = entityManager.find(GroupMessage.class, id);
        entityManager.remove(message);
    }

    public int inboxCount(String id) {
        String jpql = "select count(m.id) from PersonalMessage m where m.to = ?1";
        return getCount(id, jpql);
    }

    public int sentCount(String id) {
        String jpql = "select count(m.id) from PersonalMessage m where m.from = ?1";
        return getCount(id, jpql);
    }

    public List<PersonalMessage> get30Messages(String account1, String account2, int offset) {
        String jpql = "select m from PersonalMessage m where (m.from=:account1 and m.to=:account2) or (m.from=:account2 and m.to=:account1) order by m.date desc";
        Query query = entityManager.createQuery(jpql);
        query.setParameter("account1", new Account(account1));
        query.setParameter("account2", new Account(account2));
        query.setMaxResults(30);
        query.setFirstResult(offset);
        return query.getResultList();
    }

    private int getCount(String id, String jpql) {
        Account account = new Account();
        account.setId(id);
        Query query = entityManager.createQuery(jpql);
        query.setParameter(1, account);
        Long object = (Long) query.getSingleResult();
        return object == null ? 0 : object.intValue();
    }
}

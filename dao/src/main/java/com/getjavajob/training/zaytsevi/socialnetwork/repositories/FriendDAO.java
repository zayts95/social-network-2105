package com.getjavajob.training.zaytsevi.socialnetwork.repositories;


import com.getjavajob.training.zaytsevi.socialnetwork.models.enums.Relation;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Repository
public class FriendDAO {

    @PersistenceContext
    private EntityManager entityManager;

    public void addFriendRequest(String fromId, String toId) {
        String sql = "INSERT INTO requests (from_id, to_id) values (:fromId, :toId)";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("fromId", fromId);
        query.setParameter("toId", toId);
        query.executeUpdate();
    }

    public void acceptFriendRequest(String globalId, String accountId) {
        String sql = "INSERT INTO friends (account, friend) values (:globalId, :accountId), (:accountId, :globalId)";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("globalId", globalId);
        query.setParameter("accountId", accountId);
        query.executeUpdate();
        deleteFriendRequest(accountId, globalId);
    }

    public void deleteFriendRequest(String fromId, String toId) {
        String sql = "DELETE FROM requests WHERE from_id = :from and to_id = :to ";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("from", fromId);
        query.setParameter("to", toId);
        query.executeUpdate();
    }

    public void removeFriend(String globalId, String accountId) {
        String sql = "DELETE FROM friends WHERE (account = :accountId and friend = :globalId) or (account = :globalId and friend = :accountId) ";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("globalId", globalId);
        query.setParameter("accountId", accountId);
        query.executeUpdate();
    }

    public Relation relation(String globalId, String accountId) {
        String checkFriend = "select count(*) from friends where account = ?1 and friend = ?2";
        String checkRelation = "select count(*) from requests where from_id = ?1 and to_id = ?2";
        if (UtilsDAO.checkExistence(entityManager, checkFriend, globalId, accountId)) {
            return Relation.FRIEND;
        } else if (UtilsDAO.checkExistence(entityManager, checkRelation, globalId, accountId)) {
            return Relation.REQUEST_SENT;
        } else if (UtilsDAO.checkExistence(entityManager, checkRelation, accountId, globalId)) {
            return Relation.REQUESTED;
        } else {
            return Relation.NONE;
        }
    }

    public void cancelRequest(String globalId, String accountId) {
        String sql = "DELETE FROM requests WHERE from_id = :globalId and to_id = :accountId";
        Query query = entityManager.createNativeQuery(sql);
        query.setParameter("globalId", globalId);
        query.setParameter("accountId", accountId);
        query.executeUpdate();
    }
}
package com.getjavajob.training.zaytsevi.socialnetwork;

import com.getjavajob.training.zaytsevi.socialnetwork.configuration.DaoTestConfig;
import com.getjavajob.training.zaytsevi.socialnetwork.models.*;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.AccountDAO;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.GroupRepository;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.MessageDAO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(SpringExtension.class)
@Transactional
@Import(DaoTestConfig.class)
class MessageDAOTest {

    @Autowired
    GroupRepository groupRepository;
    @Autowired
    AccountDAO accountDAO;
    @Autowired
    MessageDAO messageDAO;
    @Autowired
    JdbcTemplate jdbcTemplate;
    @PersistenceContext
    EntityManager entityManager;

    Account ivan;
    Account anna;

    Group group;

    PersonalMessage personalMessage;
    GroupMessage groupMessage;
    ProfileMessage profileMessage;

    @BeforeEach
    void initialization() {
        entityManager.createNativeQuery("delete from friends").executeUpdate();
        entityManager.createNativeQuery("delete from accounts").executeUpdate();
        entityManager.createNativeQuery("delete from gr_acc").executeUpdate();
        entityManager.createNativeQuery("delete from groups").executeUpdate();
        entityManager.createNativeQuery("delete from personal_messages").executeUpdate();
        entityManager.createNativeQuery("delete from group_messages").executeUpdate();
        entityManager.createNativeQuery("delete from profile_messages").executeUpdate();
        entityManager.flush();

        ivan = new Account("z", "adadad", "Ivan", "Zaytsev", "zayts95@gmail.com");
        anna = new Account("f", "adadad", "Anna", "Feller", "feller@gmail.com");
        ivan.setFriends(new ArrayList<>());
        ivan.getFriends().add(anna);

        accountDAO.addAccount(anna);
        accountDAO.addAccount(ivan);

        group = new Group("9gag", "9gag", "9gag", ivan);
        groupRepository.save(group);
        groupRepository.addMember(group.getId(), ivan.getId());
        entityManager.flush();

        groupMessage = new GroupMessage(ivan, group, "groupMessage", null);
        groupMessage.setId(1);
        personalMessage = new PersonalMessage(ivan, anna, "personalMessage", null);
        personalMessage.setId(2);
        profileMessage = new ProfileMessage(ivan, anna, "profileMessage", null);
        profileMessage.setId(3);
        messageDAO.createNewMessage(groupMessage);
        messageDAO.createNewMessage(personalMessage);
        messageDAO.createNewMessage(profileMessage);

        entityManager.flush();
        System.out.println("done");
    }


    @Test
    void createNewMessage() {
        Message message = new PersonalMessage(anna, ivan, "new message from Anna", null);
        messageDAO.createNewMessage(message);
        entityManager.flush();
        List<PersonalMessage> messages = messageDAO.get10MessagesFromSmn(anna.getId(), 0);
        assertEquals(message.getText(), messages.get(0).getText());
    }

    @Test
    void get10MessagesFromSmn() {
        List<PersonalMessage> messages = messageDAO.get10MessagesFromSmn(ivan.getId(), 0);
        assertEquals("personalMessage", messages.get(0).getText());
    }

    @Test
    void get10MessagesToSmn() {
        List<PersonalMessage> messages = messageDAO.get10MessagesToSmn(anna.getId(), 0);
        assertEquals("personalMessage", messages.get(0).getText());
    }

    @Test
    void getAllMessagesToSmn() {
        List<ProfileMessage> messages = messageDAO.getAllMessagesToSmn(anna);
        assertEquals("profileMessage", messages.get(0).getText());
    }

    @Test
    void getAllMessagesToGroup() {
        List<GroupMessage> messages = messageDAO.getAllMessagesToGroup(group);
        assertEquals("groupMessage", messages.get(0).getText());
    }

    @Test
    void getMessageById() {
        assertEquals("personalMessage", messageDAO.getMessageById(2).getText());
    }

    @Test
    void removeMessageById() {
        assertNull(messageDAO.getMessageById(2));
    }

    @Test
    void inboxCount() {
        assertEquals(1, messageDAO.inboxCount(anna.getId()));
    }

    @Test
    void sentCount() {
        assertEquals(1, messageDAO.sentCount(ivan.getId()));
    }
}

package com.getjavajob.training.zaytsevi.socialnetwork;

import com.getjavajob.training.zaytsevi.socialnetwork.configuration.DaoTestConfig;
import com.getjavajob.training.zaytsevi.socialnetwork.models.Account;
import com.getjavajob.training.zaytsevi.socialnetwork.models.Group;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.AccountDAO;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.GroupRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@Transactional
@Import(DaoTestConfig.class)
class GroupRepositoryTest {

    @Autowired
    GroupRepository groupRepository;
    @Autowired
    AccountDAO accountDAO;
    @Autowired
    JdbcTemplate jdbcTemplate;
    @PersistenceContext
    EntityManager entityManager;

    Account ivan;
    Account anna;

    Group group;

    @BeforeEach
    void initialization() {
        entityManager.createNativeQuery("delete from friends").executeUpdate();
        entityManager.createNativeQuery("delete from accounts").executeUpdate();
        entityManager.createNativeQuery("delete from gr_acc").executeUpdate();
        entityManager.createNativeQuery("delete from groups").executeUpdate();
        entityManager.flush();
        ivan = new Account("z", "adadad", "Ivan", "Zaytsev", "zayts95@gmail.com");
        anna = new Account("f", "adadad", "Anna", "Feller", "feller@gmail.com");
        ivan.setFriends(new ArrayList<>());
        ivan.getFriends().add(anna);
        accountDAO.addAccount(anna);
        accountDAO.addAccount(ivan);
        group = new Group("9gag", "9gag", "9gag", ivan);
        groupRepository.save(group);
        groupRepository.addMember(group.getId(), ivan.getId());
        entityManager.flush();
        System.out.println("done");
    }

    @Test
    void addMember() {
        groupRepository.addMember(group.getId(), anna.getId());
        entityManager.flush();
        assertTrue(groupRepository.member(anna.getId(), group.getId()));
    }

    @Test
    void removeMember() {
        groupRepository.removeMember(group.getId(), ivan.getId());
        entityManager.flush();
        assertFalse(groupRepository.member(ivan.getId(), group.getId()));
    }

    @Test
    void member() {
        assertTrue(groupRepository.member(ivan.getId(), group.getId()));
    }
}
package com.getjavajob.training.zaytsevi.socialnetwork;

import com.getjavajob.training.zaytsevi.socialnetwork.configuration.DaoTestConfig;
import com.getjavajob.training.zaytsevi.socialnetwork.models.Account;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.AccountDAO;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@Transactional
@Import(DaoTestConfig.class)
class AccountDAOTest {

    @Autowired
    AccountDAO accountDAO;
    @PersistenceContext
    EntityManager entityManager;

    Account ivan;
    Account anna;

    @BeforeEach
    void initialization() {
        entityManager.flush();
        ivan = new Account("z", "adadad", "Ivan", "Zaytsev", "zayts95@gmail.com");
        anna = new Account("f", "adadad", "Anna", "Feller", "feller@gmail.com");
        ivan.setFriends(new ArrayList<>());
        ivan.getFriends().add(anna);
        accountDAO.addAccount(anna);
        accountDAO.addAccount(ivan);
        entityManager.flush();
    }

    @AfterEach
    void deletion() {
        entityManager.createNativeQuery("delete from accounts").executeUpdate();
        entityManager.createNativeQuery("delete from friends").executeUpdate();
    }

    @Test
    void getAccountByEmail() {
        Account account = accountDAO.getAccountByEmail("zayts95@gmail.com");
        assertEquals("z", account.getId());
    }

    @Test
    void updateImageUrl() {
        accountDAO.updateImageUrl("z", "url");
        entityManager.flush();
        Account account = accountDAO.getAccountById("z");
        assertEquals("url", account.getImageUrl());
    }

    @Test
    void find10Accounts() {
        List<Account> accounts = accountDAO.find10Accounts("%an%", 0);
        assertEquals(2, accounts.size());
        assertTrue(accounts.contains(ivan));
        assertTrue(accounts.contains(anna));
    }

    @Test
    void checkLogin() {
        assertEquals("z", accountDAO.checkLogin("zayts95@gmail.com", "adadad"));
    }


    @Test
    void addAccount() {
        Account expected = new Account("a", "a", "a", "a", "a");
        accountDAO.addAccount(expected);
        entityManager.flush();
        assertEquals(expected, accountDAO.getAccountById("a"));
    }

    @Test
    void getAccountById() {
        ivan.setGroups(new ArrayList<>());
        ivan.setFriends(Arrays.asList(anna));
        Account account = accountDAO.getAccountById("z");
        assertEquals(ivan, account);
    }

    @Test
    void updateAccount() {
        Account expected = new Account("z", "adadad", "I", "Zaytsev", "zayts95@gmail.com");
        Account account = entityManager.find(Account.class, expected.getId());
        accountDAO.updateAccount(expected, account);
        entityManager.flush();
        Account actual = entityManager.find(Account.class, expected.getId());
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getPassword(), actual.getPassword());
        assertEquals(expected.getName(), actual.getName());
        assertEquals(expected.getSurname(), actual.getSurname());
        assertEquals(expected.getEmail(), actual.getEmail());
    }

    @Test
    void getAccountInfo() {
        assertEquals(ivan, accountDAO.getAccountById("z"));
    }
}

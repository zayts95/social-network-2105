package com.getjavajob.training.zaytsevi.socialnetwork.repositories;

import com.getjavajob.training.zaytsevi.socialnetwork.configuration.DaoTestConfig;
import com.getjavajob.training.zaytsevi.socialnetwork.models.enums.Relation;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import static org.junit.jupiter.api.Assertions.*;


@ExtendWith(SpringExtension.class)
@Transactional
@Import(DaoTestConfig.class)
@Sql(value = "classpath:InitializeFriends.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:DeleteFriends.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
class FriendDAOTest {

    public static final String CHECK_EXISTENCE_FRIENDS = "SELECT COUNT(*) FROM friends WHERE account = ?1 AND friend = ?2";
    public static final String CHECK_EXISTENCE_REQUESTS = "SELECT COUNT(*) FROM requests WHERE from_id = ?1 AND to_id = ?2";
    @Autowired
    FriendDAO friendsDAO;
    @PersistenceContext
    EntityManager entityManager;

    @Test
    void addFriendRequest() {
        friendsDAO.addFriendRequest("n", "f");
        assertTrue(UtilsDAO.checkExistence(entityManager, CHECK_EXISTENCE_REQUESTS, "n", "f"));
    }

    @Test
    void acceptFriendRequest() {
        friendsDAO.acceptFriendRequest("z", "n");
        assertTrue(UtilsDAO.checkExistence(entityManager, CHECK_EXISTENCE_FRIENDS, "n", "z"));
        assertTrue(UtilsDAO.checkExistence(entityManager, CHECK_EXISTENCE_FRIENDS, "z", "n"));
        assertFalse(UtilsDAO.checkExistence(entityManager, CHECK_EXISTENCE_REQUESTS, "n", "z"));
    }

    @Test
    void deleteFriendRequest() {
        friendsDAO.deleteFriendRequest("n", "z");
        assertFalse(UtilsDAO.checkExistence(entityManager, CHECK_EXISTENCE_REQUESTS, "n", "f"));
    }

    @Test
    void removeFriend() {
        friendsDAO.removeFriend("z", "f");
        assertFalse(UtilsDAO.checkExistence(entityManager, CHECK_EXISTENCE_FRIENDS, "f", "z"));
        assertFalse(UtilsDAO.checkExistence(entityManager, CHECK_EXISTENCE_FRIENDS, "z", "f"));
    }

    @Test
    void relation() {
        assertEquals(Relation.NONE, friendsDAO.relation("f", "n"));
        assertEquals(Relation.FRIEND, friendsDAO.relation("f", "z"));
        assertEquals(Relation.REQUEST_SENT, friendsDAO.relation("n", "z"));
        assertEquals(Relation.REQUESTED, friendsDAO.relation("z", "n"));
    }

    @Test
    void cancelRequest() {
        friendsDAO.cancelRequest("n", "z");
        assertFalse(UtilsDAO.checkExistence(entityManager, CHECK_EXISTENCE_REQUESTS, "n", "z"));
    }
}
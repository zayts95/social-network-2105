package com.getjavajob.training.zaytsevi.socialnetwork.services;

import com.getjavajob.training.zaytsevi.socialnetwork.models.*;
import com.getjavajob.training.zaytsevi.socialnetwork.models.dto.MessageDto;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.MessageDAO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MessageServiceTest {
    private static final Account IVAN = new Account("z", "adadad", "Ivan", "Zaytsev", "zayts95@gmail.com");
    private static final Account ANNA = new Account("f", "adadad", "Anna", "Feller", "feller@gmail.com");
    private static final Group GROUP1 = new Group("abc", "abc", "abc", IVAN);
    private static final Group GROUP2 = new Group("bcd", "bcd", "bcd", IVAN);
    private static final GroupMessage GROUP_MESSAGE_1 = new GroupMessage(IVAN, GROUP1, "groupMessage1", null);
    private static final GroupMessage GROUP_MESSAGE_2 = new GroupMessage(IVAN, GROUP1, "groupMessage2", null);
    private static final PersonalMessage PERSONAL_MESSAGE_1 = new PersonalMessage(IVAN, ANNA, "personalMessage1", null);
    private static final PersonalMessage PERSONAL_MESSAGE_2 = new PersonalMessage(IVAN, ANNA, "personalMessage2", null);
    private static final ProfileMessage PROFILE_MESSAGE_1 = new ProfileMessage(IVAN, ANNA, "profileMessage1", null);
    private static final ProfileMessage PROFILE_MESSAGE_2 = new ProfileMessage(IVAN, ANNA, "profileMessage2", null);
    @Mock
    MessageDAO messageDAO;
    @InjectMocks
    MessageService messageService;

    @Test
    void createNewMessage() {
        messageService.createNewMessage(GROUP_MESSAGE_1);
        verify(messageDAO).createNewMessage(GROUP_MESSAGE_1);
    }

    @Test
    void get10MessagesToSmn() {
        List<PersonalMessage> messages = Arrays.asList(PERSONAL_MESSAGE_1, PERSONAL_MESSAGE_2);
        when(messageDAO.get10MessagesToSmn("f", 0)).thenReturn(messages);
        assertEquals(messages, messageService.get10MessagesToSmn("f", 1));
    }

    @Test
    void get10MessagesFromSmn() {
        List<PersonalMessage> messages = Arrays.asList(PERSONAL_MESSAGE_1, PERSONAL_MESSAGE_2);
        when(messageDAO.get10MessagesFromSmn("z", 0)).thenReturn(messages);
        assertEquals(messages, messageService.get10MessagesFromSmn("z", 1));
    }

    @Test
    void getAllMessagesToSmn() {
        List<ProfileMessage> messages = Arrays.asList(PROFILE_MESSAGE_1, PROFILE_MESSAGE_2);
        when(messageDAO.getAllMessagesToSmn(ANNA)).thenReturn(messages);
        assertEquals(messages, messageService.getAllMessagesToSmn(ANNA));
    }

    @Test
    void getAllMessagesToGroup() {
        List<ProfileMessage> messages = Arrays.asList(PROFILE_MESSAGE_1, PROFILE_MESSAGE_2);
        when(messageDAO.getAllMessagesToSmn(ANNA)).thenReturn(messages);
        assertEquals(messages, messageService.getAllMessagesToSmn(ANNA));
    }

    @Test
    void getMessageById() {
        when(messageDAO.getMessageById(1)).thenReturn(PERSONAL_MESSAGE_1);
        assertEquals(PERSONAL_MESSAGE_1, messageService.getMessageById(1));
    }

    @Test
    void removeMessageById() {
        messageService.removeMessageById(1);
        verify(messageDAO).removeMessageById(1);
    }

    @Test
    void inboxCount() {
        when(messageDAO.inboxCount("f")).thenReturn(2);
        assertEquals(2, messageService.inboxCount("f"));
    }

    @Test
    void sentCount() {
        when(messageDAO.sentCount("z")).thenReturn(2);
        assertEquals(2, messageService.sentCount("z"));
    }

    @Test
    void get30Messages() {
        List<MessageDto> jsonMessages = Arrays.asList(new MessageDto(PERSONAL_MESSAGE_1), new MessageDto(PERSONAL_MESSAGE_2));
        when(messageDAO.get30Messages("a", "z", 0)).thenReturn(Arrays.asList(PERSONAL_MESSAGE_1, PERSONAL_MESSAGE_2));
        assertEquals(jsonMessages, messageService.get30Messages("a", "z", 0));
    }
}
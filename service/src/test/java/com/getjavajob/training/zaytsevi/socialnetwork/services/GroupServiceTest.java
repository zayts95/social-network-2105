package com.getjavajob.training.zaytsevi.socialnetwork.services;

import com.getjavajob.training.zaytsevi.socialnetwork.models.Account;
import com.getjavajob.training.zaytsevi.socialnetwork.models.Group;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.GroupRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GroupServiceTest {

    private static final Account IVAN = new Account("z", "adadad", "Ivan", "Zaytsev", "zayts95@gmail.com");
    private static final Account ANNA = new Account("f", "adadad", "Anna", "Feller", "feller@gmail.com");
    private static final Group GROUP1 = new Group("abc", "abc", "abc", IVAN);
    private static final Group GROUP2 = new Group("bcd", "bcd", "bcd", IVAN);
    @Mock
    GroupRepository groupRepository;
    @InjectMocks
    GroupService groupService;

    @Test
    void find10Groups() {
        List<Group> groups = Arrays.asList(GROUP1, GROUP2);
        when(groupRepository.find10Groups("%bc%", 0)).thenReturn(groups);
        assertEquals(groups, groupService.find10Groups("bc", 1));
    }

    @Test
    void checkMember() {
        when(groupRepository.member("z", "abc")).thenReturn(true);
        assertTrue(groupService.checkMember("z", "abc"));
    }

    @Test
    void addMember() {
        groupService.addMember("abc", "z");
        verify(groupRepository).addMember("abc", "z");
    }

    @Test
    void removeMember() {
        groupService.removeMember("abc", "z");
        verify(groupRepository).removeMember("abc", "z");
    }

    @Test
    void updateGroup() {
        when(groupRepository.findById("abc")).thenReturn(Optional.of(GROUP1));
        groupService.updateGroup(GROUP1);
        verify(groupRepository).save(GROUP1);
    }

    @Test
    void addGroup() {
        groupService.addGroup(GROUP1);
        verify(groupRepository).save(GROUP1);
    }
}
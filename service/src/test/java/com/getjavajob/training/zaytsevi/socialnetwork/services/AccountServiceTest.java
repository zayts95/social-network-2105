package com.getjavajob.training.zaytsevi.socialnetwork.services;

import com.getjavajob.training.zaytsevi.socialnetwork.models.Account;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.AccountDAO;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.PhoneDAO;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountServiceTest {

    private static final Account IVAN = new Account("z", "adadad", "Ivan", "Zaytsev", "zayts95@gmail.com");
    private static final Account ANNA = new Account("f", "adadad", "Anna", "Feller", "feller@gmail.com");
    @Mock
    AccountDAO accountDAO;
    @Mock
    PhoneDAO phoneDAO;
    @InjectMocks
    AccountService accountService;

    @Test
    void find10Accounts() {
        List<Account> accounts = Arrays.asList(ANNA, IVAN);
        when(accountDAO.find10Accounts("%an%", 0)).thenReturn(accounts);
        assertEquals(accounts, accountService.find10Accounts("an", 1));
    }

    @Test
    void updateAccount() {
        when(accountDAO.getAccountById("z")).thenReturn(IVAN);
        accountService.updateAccount(IVAN);
        verify(accountDAO).updateAccount(IVAN, IVAN);
        verify(phoneDAO).removePhones(IVAN.getPhones());
        verify(phoneDAO).addPhones(IVAN.getPhones());
    }

    @Test
    void getAccountByEmail() {
        when(accountDAO.getAccountByEmail("zayts95@gmail.com")).thenReturn(IVAN);
        assertEquals(IVAN, accountService.getAccountByEmail("zayts95@gmail.com"));
    }

    @Test
    void asyncSearch() {
        List<Account> accounts = Arrays.asList(ANNA, IVAN);
        when(accountDAO.find10Accounts("%an%", 0)).thenReturn(accounts);
        assertEquals(accounts, accountService.find10Accounts("an", 1));
    }

    @Test
    void getAccountInfo() {
        when(accountDAO.getAccountById("z")).thenReturn(IVAN);
        assertEquals(IVAN, accountService.getAccountInfo("z"));
    }

/*    @Test
    void friend() {
        when(accountDAO.friend("z", "f")).thenReturn(true);
        assertTrue(accountService.friend("z", "f"));
    }*/

    @Test
    void getDialogs() {
        List<Account> accounts = Arrays.asList(ANNA);
        when(accountDAO.getDialogs("z")).thenReturn(accounts);
        assertEquals(accounts, accountService.getDialogs("z"));
    }
}
package com.getjavajob.training.zaytsevi.socialnetwork.services;

import com.getjavajob.training.zaytsevi.socialnetwork.models.*;
import com.getjavajob.training.zaytsevi.socialnetwork.models.dto.MessageDto;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.AwsDAO;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.MessageDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.imageio.ImageIO;
import javax.servlet.http.Part;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService {

    private final MessageDAO messageDAO;
    private final AwsDAO awsDAO;

    @Autowired
    MessageService(MessageDAO messageDAO, AwsDAO awsDAO) {
        this.messageDAO = messageDAO;
        this.awsDAO = awsDAO;
    }

    @Transactional
    public void createNewMessage(Message message) {
        messageDAO.createNewMessage(message);
    }

    @Transactional
    public List<PersonalMessage> get10MessagesToSmn(String to, int page) {
        int offset;
        if (page < 1) {
            offset = 0;
        } else {
            offset = (page - 1) * 10;
        }
        return messageDAO.get10MessagesToSmn(to, offset);
    }

    @Transactional
    public List<PersonalMessage> get10MessagesFromSmn(String from, int page) {
        int offset;
        if (page < 1) {
            offset = 0;
        } else {
            offset = (page - 1) * 10;
        }
        return messageDAO.get10MessagesFromSmn(from, offset);
    }

    @Transactional
    public List<ProfileMessage> getAllMessagesToSmn(Account to) {
        return messageDAO.getAllMessagesToSmn(to);
    }

    @Transactional
    public List<GroupMessage> getAllMessagesToGroup(Group to) {
        return messageDAO.getAllMessagesToGroup(to);
    }

    @Transactional
    public PersonalMessage getMessageById(long id) {
        return messageDAO.getMessageById(id);
    }

    @Transactional
    public void removeMessageById(long id) {
        messageDAO.removeMessageById(id);
    }

    @Transactional
    public void removeGroupMessageById(long id) {
        messageDAO.removeGroupMessageById(id);
    }

    @Transactional
    public int inboxCount(String accountId) {
        return messageDAO.inboxCount(accountId);
    }

    @Transactional
    public int sentCount(String accountId) {
        return messageDAO.sentCount(accountId);
    }

    @Transactional
    public List<MessageDto> get30Messages(String account1, String account2, int offset) {
        List<PersonalMessage> preResult = messageDAO.get30Messages(account1, account2, offset);
        List<MessageDto> result = new ArrayList<>();
        for (int i = preResult.size() - 1; i >= 0; i--) {
            result.add(new MessageDto(preResult.get(i)));
        }
        return result;
    }

    public String uploadImageFromPart(String accountId, Part part) throws IOException {
        if (part.getSize() == 0) {
            return null;
        }
        byte[] buffer = getImageBuffer(part);
        InputStream is = new ByteArrayInputStream(buffer);
        return uploadImage(accountId, is, buffer.length);
    }

    public String uploadImage(String globalId, InputStream image, int contentLength) {
        return awsDAO.uploadImage(globalId + System.currentTimeMillis(), image, contentLength);
    }

    private byte[] getImageBuffer(Part part) throws IOException {
        InputStream inputStream = part.getInputStream();
        BufferedImage image = ImageIO.read(inputStream);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", os);
        return os.toByteArray();
    }
}

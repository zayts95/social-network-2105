package com.getjavajob.training.zaytsevi.socialnetwork.services;

import com.getjavajob.training.zaytsevi.socialnetwork.models.enums.Relation;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.FriendDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class FriendService {

    private final FriendDAO friendDAO;

    @Autowired
    public FriendService(FriendDAO friendDAO) {
        this.friendDAO = friendDAO;
    }

    @Transactional
    public void addFriendRequest(String fromId, String toId) {
        friendDAO.addFriendRequest(fromId, toId);
    }

    @Transactional
    public void acceptFriendRequest(String accountId, String friendId) {
        friendDAO.acceptFriendRequest(accountId, friendId);
    }

    @Transactional
    public void declineFriendRequest(String fromId, String toId) {
        friendDAO.deleteFriendRequest(fromId, toId);
    }

    @Transactional
    public void removeFriend(String accountId, String friendId) {
        friendDAO.removeFriend(accountId, friendId);
    }

    @Transactional
    public Relation relation(String globalId, String accountId) {
        return friendDAO.relation(globalId, accountId);
    }

    @Transactional
    public void cancelRequest(String globalId, String accountId) {
        friendDAO.cancelRequest(globalId, accountId);
    }
}

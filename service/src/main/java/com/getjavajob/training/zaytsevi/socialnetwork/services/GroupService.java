package com.getjavajob.training.zaytsevi.socialnetwork.services;

import com.getjavajob.training.zaytsevi.socialnetwork.models.Group;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.GroupRepository;
import org.apache.commons.io.IOUtils;
import org.hibernate.Hibernate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Optional;

@Service
public class GroupService {

    private static final Logger logger = LoggerFactory.getLogger(GroupService.class);

    private final GroupRepository groupRepository;

    @Autowired
    public GroupService(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }


    @Transactional
    public List<Group> find10Groups(String search, int page) {
        int offset;
        if (page < 1) {
            offset = 0;
        } else {
            offset = (page - 1) * 10;
        }
        String s = ("%" + search + "%").toLowerCase();
        return groupRepository.find10Groups(s, offset);
    }

    @Transactional
    public Group getGroupById(String id) {
        Optional<Group> optionalGroup = groupRepository.findById(id);
        if (optionalGroup.isPresent()) {
            Group group = optionalGroup.get();
            Hibernate.initialize(group.getMembers());
            return group;
        } else {
            return null;
        }
    }

    @Transactional
    public void addGroup(Group group) {
        groupRepository.save(group);
    }

    @Transactional
    public void updateGroup(Group group) {
        Optional<Group> optionalGroup = groupRepository.findById(group.getId());
        if (optionalGroup.isPresent()) {
            Group entityGroup = optionalGroup.get();
            entityGroup.setName(group.getName());
            entityGroup.setDesc(group.getDesc());
            groupRepository.save(entityGroup);
        }
    }

    @Transactional
    public void updateImage(String groupId, InputStream image) {
        Optional<Group> optionalGroup = groupRepository.findById(groupId);
        if (optionalGroup.isPresent()) {
            Group group = optionalGroup.get();
            try {
                byte[] img = IOUtils.toByteArray(image);
                group.setImage(img);
            } catch (IOException e) {
                e.printStackTrace();
            }
            groupRepository.save(group);
        }
    }

    @Transactional
    public void addMember(String groupId, String accountId) {
        groupRepository.addMember(groupId, accountId);
    }

    @Transactional
    public void removeMember(String groupId, String accountId) {
        groupRepository.removeMember(groupId, accountId);
    }

    @Transactional
    public boolean checkMember(String accountId, String groupId) {
        return groupRepository.member(accountId, groupId);
    }
}

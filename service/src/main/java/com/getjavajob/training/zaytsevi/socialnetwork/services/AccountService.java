package com.getjavajob.training.zaytsevi.socialnetwork.services;

import com.getjavajob.training.zaytsevi.socialnetwork.models.Account;
import com.getjavajob.training.zaytsevi.socialnetwork.models.dto.AccountDto;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.AccountDAO;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.AwsDAO;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.PhoneDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
public class AccountService {

    private final AccountDAO accountDAO;
    private final PhoneDAO phoneDAO;
    private final AwsDAO awsDAO;

    @Autowired
    public AccountService(AccountDAO accountDAO, PhoneDAO phoneDAO, AwsDAO awsDAO) {
        this.accountDAO = accountDAO;
        this.phoneDAO = phoneDAO;
        this.awsDAO = awsDAO;
    }

    @Transactional
    public void removeAccount(String accountId) {
        accountDAO.removeAccount(accountId);
    }

    @Transactional
    public List<Account> find10Accounts(String search, int page) {
        int offset;
        if (page < 1) {
            offset = 0;
        } else {
            offset = (page - 1) * 10;
        }
        String s = ("%" + search + "%").toLowerCase();
        return accountDAO.find10Accounts(s, offset);
    }

    @Transactional
    public Account getAccountByEmail(String email) {
        return accountDAO.getAccountByEmail(email);
    }

    @Transactional
    public List<AccountDto> asyncSearch(String search) {
        List<Account> preResults = find10Accounts(search, 1);
        List<AccountDto> results = new ArrayList<>();
        for (Account a : preResults) {
            results.add(new AccountDto(a));
        }
        return results;
    }

    @Transactional
    public void uploadAvatar(String accountId, String key, InputStream inputStream, int contentLength) {
        String url = awsDAO.uploadImage(key, inputStream, contentLength);
        accountDAO.updateImageUrl(accountId, url);
    }

    @Transactional
    public Account getAccountById(String id) {
        Account account = accountDAO.getAccountById(id);
        account.getFriends().size();
        account.getGroups().size();
        return account;
    }

    @Transactional
    public Account getAccountWithFriends(String id) {
        Account account = accountDAO.getAccountById(id);
        account.getFriends().size();
        account.getFriendRequests().size();
        return account;
    }

    @Transactional
    public Account getAccountInfo(String id) {
        return accountDAO.getAccountById(id);
    }

    @Transactional
    public void createNewAccount(Account account) {
        accountDAO.addAccount(account);
    }

    @Transactional
    public void updateAccount(Account newAccount) {
        Account entityAccount = accountDAO.getAccountById(newAccount.getId());
        accountDAO.updateAccount(newAccount, entityAccount);
        phoneDAO.removePhones(entityAccount.getPhones());
        phoneDAO.addPhones(newAccount.getPhones());
    }

    @Transactional
    public void mergeAccount(Account account) {
        accountDAO.mergeAccount(account);
    }

    @Transactional
    public List<Account> getDialogs(String globalId) {
        return accountDAO.getDialogs(globalId);
    }
}
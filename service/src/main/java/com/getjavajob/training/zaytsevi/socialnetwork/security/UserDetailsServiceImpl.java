package com.getjavajob.training.zaytsevi.socialnetwork.security;

import com.getjavajob.training.zaytsevi.socialnetwork.models.Account;
import com.getjavajob.training.zaytsevi.socialnetwork.repositories.AccountDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service("userDetailsServiceImpl")
public class UserDetailsServiceImpl implements UserDetailsService {

    private static final Logger logger = LoggerFactory.getLogger(UserDetailsServiceImpl.class);

    private final AccountDAO accountDAO;

    @Autowired
    public UserDetailsServiceImpl(AccountDAO accountDAO) {
        this.accountDAO = accountDAO;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        logger.debug(email);
        try {
            Account account = accountDAO.getAccountByEmail(email);
            logger.debug(account.getId());
            return SecurityUser.fromAccount(account);
        } catch (Exception e) {
            logger.debug("login failed: ", e);
            e.printStackTrace();
            throw new UsernameNotFoundException("User doesn't exists");
        }
    }
}
